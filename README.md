# tasksList JS exercise

Add a task, remove a task, filter through tasks.
App is chained with the local storage.

**demo**: https://yolo_dev.gitlab.io/tasksList/
